using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilController : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(KMS());
    }

    IEnumerator KMS()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
