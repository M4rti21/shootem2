using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy: MonoBehaviour
{
    public enum SwitchMachineStates { NONE, IDLE, WALK, ATTACK, DEAD };

    //---BODY---//
    public Rigidbody2D m_RigidBody;
    public Animator m_Animator;
    public Transform m_Transform;

    [SerializeField] public AttackController m_HitBox;
    
    //---STATE---//
    public SwitchMachineStates m_CurrentState;
    public Boolean isRotated;

    //---STATS---//
    [SerializeField] public int m_Damage = 10;
    [SerializeField] public int m_Speed = 2;
    [SerializeField] public float m_AttackRate = 2f;
    [SerializeField] public int m_Hp = 40;
    
    public Transform m_Target;

    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Transform = GetComponent<Transform>();
        m_Animator = GetComponent<Animator>();
        isRotated = false;
        m_Target = null;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Attack")) return;
        m_Hp -= other.GetComponent<AttackController>().getDmg();
        if (m_Hp <= 0)
        {
            Destroy(gameObject);
        }
    }
    public void Move()
    {
        if (!m_Target) return;

        Vector2 target = m_Target.position;
        Vector2 me = m_Transform.position;
        m_RigidBody.velocity = (target - me).normalized * m_Speed;
        
        TurnAround(target);

    }

    private void TurnAround(Vector2 target)
    {
        if (target.x < m_Transform.position.x && !isRotated)
        {
            m_Transform.Rotate(0, 180, 0);
            isRotated = true;
            return;
        }

        if (target.x > m_Transform.position.x && isRotated)
        {
            m_Transform.Rotate(0, -180, 0);
            isRotated = false;
        }
    }

}
