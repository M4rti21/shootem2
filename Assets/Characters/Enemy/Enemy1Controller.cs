using System;
using UnityEngine;

public class Enemy1Controller : MonoBehaviour
{
    private enum SwitchMachineStates { IDLE, WALK, ATTACK, DEAD };

    //---BODY---//
    private Rigidbody2D m_RigidBody;
    private Animator m_Animator;
    private Transform m_Transform;

    [SerializeField] private AttackController m_HitBox;
    
    //---STATE---//
    private SwitchMachineStates m_CurrentState;
    private Boolean isRotated;

    //---STATS---//
    [SerializeField] private int m_Damage = 10;
    [SerializeField] private int m_Speed = 2;
    [SerializeField] private float m_AttackRate = 2f;
    [SerializeField] private int m_Hp = 40;

    //---AREAS---//
    [SerializeField] private EnemyTriggerBoxController m_WalkArea;
    [SerializeField] private EnemyTriggerBoxController m_AttackArea;

    private Transform m_Target;

    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Transform = GetComponent<Transform>();
        m_Animator = GetComponent<Animator>();
        isRotated = false;
        m_Target = null;
        m_WalkArea.OnEnter += SetTarget;
        m_WalkArea.OnExit += DelTarget;
        m_AttackArea.OnEnter += StartAttack;
        m_AttackArea.OnExit += StopAttack;
    }
    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }
    private void Update()
    {
        UpdateState();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Attack")) return;
        m_Hp -= other.GetComponent<AttackController>().getDmg();
        if (m_Hp > 0) return;
        GameManager.Instance.spawner.RemoveEnemy();
        Destroy(gameObject);
    }

    private void Attack()
    {
        m_HitBox.GetComponent<AttackController>().setDmg(m_Damage);
    }

    private void SetTarget(Transform target)
    {
        m_Target = target;
        ChangeState(SwitchMachineStates.WALK);
    }

    private void DelTarget(Transform target)
    {
        ChangeState(SwitchMachineStates.IDLE);
        m_Target = null;
    }

    private void StartAttack(Transform target)
    {
        ChangeState(SwitchMachineStates.ATTACK);
    }

    private void StopAttack(Transform target)
    {
        ChangeState(SwitchMachineStates.WALK);
    }

    private void Move()
    {
        if (!m_Target) return;

        Vector2 target = m_Target.position;
        Vector2 me = m_Transform.position;
        m_RigidBody.velocity = (target - me).normalized * m_Speed;
        
        TurnAround(target);

    }

    private void TurnAround(Vector2 target)
    {
        if (target.x < m_Transform.position.x && !isRotated)
        {
            m_Transform.Rotate(0, 180, 0);
            isRotated = true;
            return;
        }

        if (!(target.x > m_Transform.position.x) || !isRotated) return;
        m_Transform.Rotate(0, -180, 0);
        isRotated = false;
    }

    private void ChangeState(SwitchMachineStates newState)
    {
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        m_Animator.speed = 1;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_RigidBody.velocity = Vector2.zero;
                m_Animator.Play("IdleAnimation");
                break;
            case SwitchMachineStates.WALK:
                m_Animator.Play("WalkAnimation");
                break;
            case SwitchMachineStates.ATTACK:
                m_RigidBody.velocity = Vector2.zero;
                m_Animator.speed = m_AttackRate;
                m_Animator.Play("AttackAnimation");
                m_HitBox.setDmg(m_Damage);
                break;
            case SwitchMachineStates.DEAD:
                m_Animator.Play("DeadAnimation");
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.WALK:
                Move();
                break;
            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.DEAD:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.WALK:
                break;
            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.DEAD:
                break;
        }
    }

}
