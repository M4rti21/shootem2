using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    private int dmg;

    public void setDmg(int dmg)
    {
        this.dmg = dmg;
    }

    public int getDmg()
    {
        return dmg;
    }

}
