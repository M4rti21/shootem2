using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private InputActionAsset p_InputAsset;
    private static InputActionAsset p_Input;

    private enum SwitchMachineStates
    {
        IDLE,
        WALK,
        RUN,
        ATTACK,
        JUMP,
        DEAD
    };

    private SwitchMachineStates m_CurrentState;
    private Rigidbody2D m_RigidBody;
    private InputAction m_Movement;
    private Animator m_Animator;
    private Transform m_Transform;

    [SerializeField] private AttackController m_HitBox;

    private bool isRotated;
    private bool isCombo;

    public static int m_Hp = 100;
    [SerializeField] private int m_Damage;
    [SerializeField] private int m_DamageCombo;
    [SerializeField] private int m_JumpForce = 3;
    [SerializeField] private int m_Speed = 3;

    [SerializeField] private GameEvent onHpChange;

    private void Awake()
    {
        p_Input = Instantiate(p_InputAsset);
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_Transform = GetComponent<Transform>();
        m_Movement = p_Input.FindActionMap("gameplay").FindAction("Move");
        InputActionMap gameplay = p_Input.FindActionMap("gameplay");
        gameplay.FindAction("Run").started += StartRun;
        gameplay.FindAction("Run").canceled += EndRun;
        gameplay.FindAction("Attack").started += StartAttack;
        gameplay.FindAction("Jump").started += StartJump;
        gameplay.Enable();
        isRotated = false;
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnDestroy()
    {
        p_Input.FindActionMap("gameplay").Disable();
    }

    private void StartCombo()
    {
        isCombo = true;
    }

    private void EndCombo()
    {
        isCombo = false;
    }

    private void StartRun(InputAction.CallbackContext context)
    {
        if (m_CurrentState == SwitchMachineStates.JUMP) return;
        ChangeState(SwitchMachineStates.RUN);
    }

    private void EndRun(InputAction.CallbackContext context)
    {
        if (m_CurrentState == SwitchMachineStates.JUMP) return;
        ChangeState(SwitchMachineStates.WALK);
    }

    private void StartAttack(InputAction.CallbackContext context)
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.JUMP:
            case SwitchMachineStates.ATTACK:
                return;
        }

        ChangeState(SwitchMachineStates.ATTACK);
    }

    private void EndAttack()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void StartJump(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.JUMP);
    }

    private void EndJump()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Attack")) return;
        m_Hp -= other.GetComponent<AttackController>().getDmg();
        onHpChange.Raise();
        Debug.Log("hurt");
        if (m_Hp > 0) return;
        InputActionMap gameplay = p_Input.FindActionMap("gameplay");
        gameplay.FindAction("Run").started -= StartRun;
        gameplay.FindAction("Run").canceled -= EndRun;
        gameplay.FindAction("Attack").started -= StartAttack;
        gameplay.FindAction("Jump").started -= StartJump;
        gameplay.Disable();
        SceneManager.LoadScene("End");
        Destroy(gameObject);
    }

    private void ChangeState(SwitchMachineStates newState)
    {
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_RigidBody.velocity = Vector2.zero;
                m_Animator.Play("IdleAnimation");
                isCombo = false;
                break;
            case SwitchMachineStates.WALK:
                m_Animator.Play("WalkAnimation");
                break;
            case SwitchMachineStates.RUN:
                m_Animator.Play("RunAnimation");
                break;
            case SwitchMachineStates.ATTACK:
                if (isCombo)
                {
                    m_Animator.Play("ComboAttackAnimation");
                    m_HitBox.setDmg(m_DamageCombo);
                    return;
                }
                m_Animator.Play("AttackAnimation");
                m_HitBox.setDmg(m_Damage);
                isCombo = true;
                break;
            case SwitchMachineStates.JUMP:
                m_Animator.Play("JumpAnimation");
                m_RigidBody.AddForce(Vector2.up * m_JumpForce, ForceMode2D.Impulse);
                break;
            case SwitchMachineStates.DEAD:
                m_Animator.Play("DeadAnimation");
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.WALK:
                break;
            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.JUMP:
                break;
            case SwitchMachineStates.DEAD:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                if (m_Movement.ReadValue<Vector2>().x != 0)
                    ChangeState(SwitchMachineStates.WALK);

                break;
            case SwitchMachineStates.WALK:

                Move(m_Speed);

                break;
            case SwitchMachineStates.RUN:

                Move(m_Speed * 2);

                break;
            case SwitchMachineStates.ATTACK:

                break;

            case SwitchMachineStates.JUMP:

                break;
            case SwitchMachineStates.DEAD:

                break;
        }
    }

    private void Move(float spd)
    {
        m_RigidBody.velocity = Vector2.right * (m_Movement.ReadValue<Vector2>().x * spd);

        if (m_RigidBody.velocity.x == 0)
        {
            ChangeState(SwitchMachineStates.IDLE);
            return;
        }

        if (m_Movement.ReadValue<Vector2>().x < 0 && !isRotated)
        {
            m_Transform.Rotate(0, 180, 0);
            isRotated = true;
            return;
        }

        if (m_Movement.ReadValue<Vector2>().x > 0 && isRotated)
        {
            m_Transform.Rotate(0, -180, 0);
            isRotated = false;
        }
    }
}