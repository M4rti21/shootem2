using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{    
    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;

    [SerializeField] public SpawnerController spawner;
    
    public int Round;
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this);
    }
}
