using TMPro;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI txt_hp;
    [SerializeField] private TextMeshProUGUI txt_round;

    private void Start()
    {
        changeHp();
        changeRound();
    }

    public void changeHp()
    {
        txt_hp.text = "HP:" + PlayerController.m_Hp;
    }
    
    public void changeRound()
    {
        txt_round.text = "Round:" + GameManager.Instance.Round;
    }
}
