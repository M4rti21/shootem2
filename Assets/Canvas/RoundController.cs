using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoundController : MonoBehaviour
{
    void Start()
    {
        this.GetComponent<TextMeshProUGUI>().text = "Round: " + GameManager.Instance.Round;
    }

    public void RestartScene()
    {
        SceneManager.LoadScene("Game");
    }
}
