using System.Collections;
using UnityEngine;
using Random = System.Random;

public class SpawnerController : MonoBehaviour
{
    private int enemies;

    [SerializeField] private GameObject Enemy1;
    [SerializeField] private GameObject Enemy2;

    private void Start()
    {
        GameManager.Instance.Round=1;
        StartCoroutine(Spawn());
    }

    public void RemoveEnemy()
    {
        enemies--;
        if (enemies > 0) return;
        GameManager.Instance.Round++;
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        var round = GameManager.Instance.Round;
        enemies = round;
        for (var i = 0; i < round; i++)
        {
            Random random = new Random();
            GameObject e = random.NextDouble() < 0.5 ? Enemy1 : Enemy2;
            Instantiate(e);
        }
        yield return new WaitForSeconds(4 / (round + 1));
    }
}
